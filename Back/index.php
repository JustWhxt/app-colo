<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\User;
use App\Controllers\Login;
use App\Controllers\Register;
use App\Controllers\Informations;
use App\Controllers\TaskAll;
use App\Controllers\Tasks;
use App\Controllers\DepenseAll;
use App\Controllers\Depenses;




new Router([
  'user/:id' => User::class,
  'login/' => Login::class,
  'register/' => Register::class,
  'informations/' => Informations::class,
  'tasks/' => TaskAll::class,
  'tasks/:id' => Tasks::class,
  'depenses/' => DepenseAll::class,
  'depenses/:id' => Depenses::class
]);
