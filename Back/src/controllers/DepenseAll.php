<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\DepensesModel;

class DepenseAll extends Controller {
  protected $depensesModel;

  public function __construct($params) {
    $this->depensesModel = new DepensesModel();
    parent::__construct($params);
  }

  public function getDepenseAll() {
    $depenses = $this->depensesModel->get();
    if ($depenses === null) {
      echo json_encode(['status' => 'fail', 'message' => 'Aucune dépense']);
    } else {
      echo json_encode($depenses);
    }
  }

  public function postDepenseAll() {
    $user = $this->body['user'] ?? 'null';
    $name = $this->body['name'] ?? 'null';
    $category = $this->body['category'] ?? 'null';
    $rising = $this->body['rising'] ?? 'null';

    $data = [
      'user' => $user,
      'name' => $name,
      'category' => $category,
      'rising' => $rising
    ];

    if ($this->validateDepenseData($data)) {
      $this->depensesModel->add($data);
      echo json_encode(['status' => 'success', 'message' => 'Dépense ajoutée avec succès']);
    } else {
      echo json_encode(['status' => 'fail', 'message' => 'Données incomplètes ou incorrectes']);
    }
}

public function validateDepenseData($data) {
  foreach ($data as $value) {
    if (empty($value)) {
        return false;
    }
  }
  return true;
}
}
?>