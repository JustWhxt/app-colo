<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\DepensesModel;

class Depenses extends Controller {
  protected $depensesModel;

  public function __construct($params) {
    $this->depensesModel = new DepensesModel();
    parent::__construct($params);
  }

  public function deleteDepenses() {
    $depenses_id = $this->params['id'];
    $deleteDepenses = $this->depensesModel->delete($depenses_id);
  }
}
?>
