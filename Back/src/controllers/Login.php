<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class Login extends Controller {
  protected $userModel;

  public function __construct($params) {
    $this->userModel = new UserModel();
    parent::__construct($params);
  }

  public function postLogin() {
    $email = $this->body['email'];
    $password = $this->body['password'];
  
    if (empty($email) || empty($password)) {
      echo json_encode(['status' => 'fail', 'message' => 'Veuillez remplir les informations']);
      return;
    }
  
    $user = $this->userModel->getByEmail($email);
    
    if ($user && password_verify($password, $user['password'])) {
      echo json_encode(['status' => 'success', 'user' => $user, 'userId' => $user['id']]);
    } else {
      echo json_encode(['status' => 'fail', 'message' => 'Identifiants invalides']);
    }
  }
}
?>
