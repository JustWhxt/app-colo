<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class Register extends Controller {
  protected $userModel;

  public function __construct($params) {
    $this->userModel = new UserModel();
    parent::__construct($params);
  }

  public function postRegister() {
    $name = $this->body['name'];
    $email = $this->body['email'];
    $password = $this->body['password'];

    if (empty($name) || empty($email) || empty($password)) {
      echo json_encode(['status' => 'fail', 'message' => 'Veuillez remplir toutes les informations.']);
      return;
    }
    if ($this->userModel->getByEmail($email)) {
      echo json_encode(['status' => 'fail', 'message' => 'Un compte avec cet email existe déjà.']);
      return;
    }

    $passwordHashed = password_hash($password, PASSWORD_DEFAULT);
    $user = [
      'name' => $name,
      'email' => $email,
      'password' => $passwordHashed
    ];
    $this->userModel->add($user);
    echo json_encode(['status' => 'success', 'message' => 'Compte créé avec succès pour ' . $user['name']]);
  }
}
?>