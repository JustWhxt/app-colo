<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\TasksModel;

class TaskAll extends Controller {
  protected $tasksModel;

  public function __construct($params) {
    $this->tasksModel = new TasksModel();
    parent::__construct($params);
  }

  public function getTaskAll() {
    $tasks = $this->tasksModel->get();
    if ($tasks === null) {
      echo json_encode(['status' => 'fail', 'message' => 'Aucune tâche']);
    } else {
      echo json_encode($tasks);
    }
  }

  public function postTaskAll() {
    $name = $this->body['name'] ?? 'null';
    $category = $this->body['category'] ?? null;
    $description = $this->body['description'] ?? null;
    $priority = $this->body['priority'] ?? null;
    $assignee = $this->body['assignee'] ?? null;
    
    $data = [
       'name' => $name,
       'category' => $category,
       'description' => $description,
       'priority' => $priority,
       'assignee' => $assignee
    ];

    if ($this->validateTaskData($data)) {
        $this->tasksModel->add($data);
        echo json_encode(['status' => 'success', 'message' => 'Tâche ajoutée avec succès']);
    } else {
        echo json_encode(['status' => 'fail', 'message' => 'Données incomplètes ou incorrectes']);
    }
  }

  public function validateTaskData($data) {
    foreach ($data as $value) {
      if (empty($value)) {
          return false;
      }
    }
    return true;
  }
}
?>