<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\TasksModel;

class Tasks extends Controller {
  protected $tasksModel;

  public function __construct($params) {
    $this->tasksModel = new TasksModel();
    parent::__construct($params);
  }

  public function deleteTasks() {
    $tasks_id = $this->params['id'];
    $deleteTasks = $this->tasksModel->delete($tasks_id);
  }
}
?>
