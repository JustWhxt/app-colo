<?php

namespace App\Models;

use \PDO;

class DepensesModel extends SqlConnect { 
  public function add(array $data) {
    $query = "
      INSERT INTO depenses (user, name, category, rising)
      VALUES (:user, :name, :category, :rising)
    ";

    $req = $this->db->prepare($query);
    $req->execute($data);
  }

  public function delete(int $id) {
    $req = $this->db->prepare("DELETE FROM depenses WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get() {
    $req = $this->db->prepare("SELECT * FROM depenses");
    $req->execute();
    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
  }
}