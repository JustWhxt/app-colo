<?php

namespace App\Models;

use \PDO;

class TasksModel extends SqlConnect { 
  public function add(array $data) {
    $date = date('Y-m-d H:i:s');
    $query = "
      INSERT INTO tasks (name, category, description, priority, assignee, date)
      VALUES (:name, :category, :description, :priority, :assignee, :date)
    ";

    $req = $this->db->prepare($query);
    $data['date'] = $date;
    $req->execute($data);
  }

  public function delete(int $id) {
    $req = $this->db->prepare("DELETE FROM tasks WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get() {
    $req = $this->db->prepare("SELECT * FROM tasks");
    $req->execute();
    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
  }
}