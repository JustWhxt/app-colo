<?php

namespace App\Models;

use \PDO;
use stdClass;

class UserModel extends SqlConnect {
    public function add(array $data) {
      $query = "
        INSERT INTO users (name, email, password)
        VALUES (:name, :email, :password)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM users WHERE id = :id");
      $req->execute(["id" => $id]);
    }

    public function getByEmailAndPassword($email, $password) {
      $req = $this->db->prepare("SELECT * FROM users WHERE email=:email AND password=:password");
      $req->execute(["email"=> $email, 'password' => $password]);
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }

    public function getName($name) {
      $req = $this->db->prepare("SELECT * FROM users WHERE name=:name");
      $req->execute(["name"=> $name]);
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }

    public function getByEmail($email) {
      $req = $this->db->prepare("SELECT * FROM users WHERE email=:email");
      $req->execute(["email"=> $email]);
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }

    public function checkEmail($email) {
      $req = $this->db->prepare("SELECT COUNT(*) FROM users WHERE email = :email");
      $req->execute(["email" => $email]);
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }
    

    public function get(int $id) {
      $req = $this->db->prepare("SELECT * FROM users WHERE id = :id");
      $req->execute(["id" => $id]);
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }
}
