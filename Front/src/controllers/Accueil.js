import axios from 'axios';
import viewNav from '../views/nav';
import viewAccueilContent from '../views/accueilContent';

const Accueil = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.run();
  }

  render() {
    return `
        ${viewNav()}
        ${viewAccueilContent()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();

    const container = document.querySelector('.container');
    const signUpButton = document.querySelector('#signUp');
    const loginUpButton = document.querySelector('#loginUp');
    const loginFormButton = document.querySelector('.login-container .btn-form');
    const signUpFormButton = document.querySelector('.sign-up-container .btn-form');

    signUpButton.addEventListener('click', () => {
      container.classList.add('panel-active');
    });

    loginUpButton.addEventListener('click', () => {
      container.classList.remove('panel-active');
    });

    loginFormButton.addEventListener('click', (e) => {
      e.preventDefault();
      const emailInput = this.el.querySelector('.loginEmail').value;
      const passwordInput = this.el.querySelector('.loginPassword').value;
      axios.post('http://localhost:6001/login', {
        email: emailInput,
        password: passwordInput
      }).then((response) => {
        if (response.data.status === 'success') {
          document.cookie = `userId=${response.data.userId}`;
          document.cookie = `userName=${response.data.userName}`;
          window.location.href = '/Dashboard';
        } else {
          const errorMessageElement = document.querySelector('.error-message');
          errorMessageElement.textContent = response.data.message || 'Vous n\'avez pas de compte avec cet email.';
        }
      });
    });

    signUpFormButton.addEventListener('click', (e) => {
      e.preventDefault();
      const nameInput = this.el.querySelector('.signupName').value;
      const emailInput = this.el.querySelector('.signupEmail').value;
      const passwordInput = this.el.querySelector('.signupPassword').value;
      axios.post('http://localhost:6001/register', {
        name: nameInput,
        email: emailInput,
        password: passwordInput
      }).then((response) => {
        if (response.data.status === 'success') {
          window.location.href = '/Dashboard';
          document.cookie = `userId=${response.data.userId}`;
        } else {
          const errorMessageElement = document.querySelector('.error-message');
          errorMessageElement.textContent = response.data.message || 'Une erreur est survenue lors de l\'inscription.';
        }
      });
    });
  }
};

export default Accueil;
