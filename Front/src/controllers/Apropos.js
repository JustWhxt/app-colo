import viewNav from '../views/nav';
import viewApropos from '../views/aPropos';

const Apropos = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.run();
  }

  render() {
    return `
        ${viewNav()}
        ${viewApropos()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Apropos;
