import axios from 'axios';
import viewNav from '../views/nav';
import viewDashboard from '../views/dashboard';
import logoDel from '../assets/img/delete.png';

const Dashboard = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.date = new Date();
    this.run();
  }

  render() {
    return `
        ${viewNav()}
        ${viewDashboard()}
    `;
  }

  run() {
    if (this.el) {
      this.el.innerHTML = this.render();
      this.getTasks();
      this.getDepenses();
      this.renderCalendar();

      this.el.addEventListener('click', (event) => {
        const { target } = event;
        if (target && (target.classList.contains('depense-completed-icon') || target.classList.contains('task-completed-icon'))) {
          const id = target.dataset.depenseId || target.dataset.taskId;
          const type = target.classList.contains('depense-completed-icon') ? 'depenses' : 'tasks';
          this.deleteItem(type, id);
        }
      });
    }
  }

  getDepenses() {
    axios.get('http://localhost:6001/depenses')
      .then((response) => {
        const depensesContainer = document.querySelector('.depenses');
        let depensesHtml = '';
        const { data } = response;
        if (Array.isArray(data)) {
          data.forEach(({
            id,
            name,
            rising,
            user,
            category
          }) => {
            depensesHtml += `
            <tr>
              <td>${name}</td>
              <td>${rising} <span>€</span></td>
              <td>${user}</td>
              <td>${category}</td>
              <td class="delete-column"><img class="depense-completed-icon" data-depense-id="${id}" src="${logoDel}" alt="LogoDelete"></td>
            </tr>
            `;
          });
        }
        if (depensesContainer) {
          depensesContainer.innerHTML = `
            <table class="table-depenses">
              <tr>
                <th>Nom de la dépense</th>
                <th>Montant</th>
                <th>Utilisateur</th>
                <th>Catégorie</th>
                <th>Action</th>
              </tr>
              ${depensesHtml}
            </table>
          `;
        }
      });
  }

  getTasks() {
    axios.get('http://localhost:6001/tasks')
      .then((response) => {
        const tasksContainer = document.querySelector('.tasks');
        let tasksHtml = '';
        const { data } = response;
        if (Array.isArray(data)) {
          data.forEach(({
            id,
            name,
            category,
            description,
            priority,
            assignee
          }) => {
            tasksHtml += `
              <div class="task-item">
                <h2>${name}</h2>
                <p>Type de tâche :<span class="task-type">${category}</span></p>
                <p>Description de la tâche : ${description}</p>
                <p class="task-priority">Priorité: ${priority}</p>
                <p>Assigné à :<span class="assignTask"> ${assignee}</span></p>
                <div class="task-deadline">
                <span class="deadline-date">08/08/2024</span>
                </div>
                <img class="task-completed-icon" data-task-id="${id}" src="${logoDel}" alt="LogoDelete">
              </div>
            `;
          });
        }
        if (tasksContainer) {
          tasksContainer.innerHTML = tasksHtml;
        }
      });
  }

  deleteItem(type, id) {
    axios.delete(`http://localhost:6001/${type}/${id}`)
      .then(() => {
        window.location.reload();
      });
  }

  renderCalendar() {
    this.date.setDate(1);

    const monthDays = document.querySelector('.days');

    const lastDay = new Date(
      this.date.getFullYear(),
      this.date.getMonth() + 1,
      0
    ).getDate();

    const prevLastDay = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      0
    ).getDate();

    const firstDayIndex = this.date.getDay();

    const lastDayIndex = new Date(
      this.date.getFullYear(),
      this.date.getMonth() + 1,
      0
    ).getDay();

    const nextDays = 7 - lastDayIndex - 1;

    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    document.querySelector('.date h1').innerHTML = months[this.date.getMonth()];

    document.querySelector('.date p').innerHTML = new Date().toDateString();

    let days = '';

    for (let x = firstDayIndex; x > 0; x -= 1) {
      days += `<div class='prev-date'>${prevLastDay - x + 1}</div>`;
    }

    for (let i = 1; i <= lastDay; i += 1) {
      if (
        i === new Date().getDate()
        && this.date.getMonth() === new Date().getMonth()
      ) {
        days += `<div class='today'>${i}</div>`;
      } else {
        days += `<div>${i}</div>`;
      }
    }

    for (let j = 1; j <= nextDays; j += 1) {
      days += `<div class='next-date'>${j}</div>`;
    }
    monthDays.innerHTML = days;
  }
};

export default Dashboard;
