import axios from 'axios';
import viewNav from '../views/nav';
import viewCreateTasks from '../views/createTasks';
import viewCreateDepenses from '../views/createDepenses';

const GestionMenu = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.run();
  }

  render() {
    return `
        ${viewNav()}
        ${viewCreateTasks()}
        ${viewCreateDepenses()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();

    const addDepense = document.querySelector('.depenses-form');
    if (addDepense) {
      addDepense.addEventListener('submit', (event) => {
        event.preventDefault();
        const nameValue = document.querySelector('.nameDepense').value;
        const risingValue = document.querySelector('.risingDepense').value;
        const userValue = document.querySelector('.userDepense').value;
        const categoryValue = document.querySelector('.categoryDepense').value;
        axios.post('http://localhost:6001/depenses', {
          name: nameValue,
          rising: risingValue,
          user: userValue,
          category: categoryValue
        });
      });
    }

    const addTask = document.querySelector('.task-form');
    if (addTask) {
      addTask.addEventListener('submit', (event) => {
        event.preventDefault();
        const nameValue = document.querySelector('.titleTask').value;
        const descriptionValue = document.querySelector('.descriptionTask').value;
        const priorityValue = document.querySelector('.priorityTask').value;
        const categoryValue = document.querySelector('.categoryTask').value;
        const assigneeValue = document.querySelector('.assignTask').value;

        axios.post('http://localhost:6001/tasks', {
          name: nameValue,
          description: descriptionValue,
          priority: priorityValue,
          category: categoryValue,
          assignee: assigneeValue
        });
      });
    }
  }
};

export default GestionMenu;
