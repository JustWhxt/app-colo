import viewNav from '../views/nav';
import viewMesInfo from '../views/mesInfos';

const Informations = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.run();
  }

  render() {
    return `
        ${viewNav()}
        ${viewMesInfo()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    const logoutBtn = document.querySelector('.btn-logout');

    logoutBtn.addEventListener('click', () => {
      document.cookie = 'userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      document.cookie = 'userName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    });
  }
};

export default Informations;
