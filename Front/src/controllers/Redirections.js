const Redirections = class {
  constructor(params) {
    this.params = params;
    this.run();
  }

  run() {
    window.location = '/Accueil';
  }
};

export default Redirections;
