import Router from './Router';
import Accueil from './controllers/Accueil';
import GestionMenu from './controllers/GestionMenu';
import Dashboard from './controllers/Dashboard';
import Apropos from './controllers/Apropos';
import Informations from './controllers/Informations';
import Redirections from './controllers/Redirections';

import './index.scss';

const routes = [{
  url: '/',
  controller: Redirections
},
{
  url: '/Accueil',
  controller: Accueil
},
{
  url: '/GestionMenu',
  controller: GestionMenu
},
{
  url: '/Dashboard',
  controller: Dashboard
},
{
  url: '/Apropos',
  controller: Apropos
},
{
  url: '/Informations',
  controller: Informations
}];

new Router(routes);
