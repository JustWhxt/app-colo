const aPropos = () => `
<div class="container-apropos">
<h1>Notre concept</h1>
<p class="subtitle-apropos">EasyColoc est là pour vous aider à vous répartir les tâches au sein de votre colocation, simplifiant la gestion quotidienne et améliorant l'harmonie entre colocataires.</p><br>
<div class="content-apropos">
    <div class="text-section-apropos">
        <h2>Bienvenue au Club</h2>
        <div class="separator-container">
            <div class="separator"></div>
        </div>
        <p>EasyColoc offre une plateforme dédiée à la création d'événements communautaires, renforçant les liens et la communication entre colocataires.</p>
        <p>« Chez EasyColoc, nous croyons que vivre ensemble est une aventure en soi. Nous nous engageons à rendre cette cohabitation aussi agréable et enrichissante que possible. »</p>
        <div class="author-apropos">
            <img src="https://media.licdn.com/dms/image/D4D03AQFSRejEexvNGg/profile-displayphoto-shrink_200_200/0/1697709903321?e=2147483647&v=beta&t=cc-KcIKaZ4aD0rAW2xC9msNjFI8sH4rpA5fu1ONGfDU" alt="Thibaud Hourman">
            <p><strong>Thibaud</strong><br>Thibaud Hourman, EasyColoc</p>
        </div>
    </div>
    <div class="image-section-apropos">
        <img src="https://media.licdn.com/dms/image/D4D03AQFSRejEexvNGg/profile-displayphoto-shrink_200_200/0/1697709903321?e=2147483647&v=beta&t=cc-KcIKaZ4aD0rAW2xC9msNjFI8sH4rpA5fu1ONGfDU" alt="Group of people exploring">
    </div>
</div>
</div>
<footer class="footer-apropos">
<p>&copy; 2024 EasyColoc. Tous droits réservés.</p>
</footer>
`;

export default aPropos;
