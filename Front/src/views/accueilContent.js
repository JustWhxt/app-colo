const accueilContent = () => `

<div class="containertexte">
    <h1 class="texte1">EASYCOLOC</h1>
    <h3 class="texte2">Marre de se prendre la tête avec tes Collocataires ?
      Inscrit toi maintenant sur notre site !</h3>
    <p class="texte3">EasyColoc c'est quoi ? C'est simple nous avons conçu ce site pour améliorer<br>la gestion de vos
      tâches
      facilement sans vous prendre la tête.<br>Il suffit simplement de vous inscrire vous et vos amis de vous
      ajouter.<br>
      Ensuite vous pourrez créer, modifier, attribué des tâches a vos colocataires.<br> Mais pas que ! EasyColoc peut
      aussi
      créer des evenements et vous dire le nombre de dépenses efféctués.
    </p>
  </div>
  <div class="error-message"></div>
  <div class="container" id="container">
  <div class="form-container sign-up-container">
    <form>
      <h1>Créer un compte</h1>
      <div class="social-container">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-google-plus-g"></i></a>
        <a href="#"><i class="fab fa-linkedin-in"></i></a>
      </div>
      <span>Utiliser compte gmail</span>
      <input type="text" class="signupName" name="name" placeholder="Nom">
      <input type="email" class="signupEmail" name="email" placeholder="Email">
      <input type="password" class="signupPassword" name="password" placeholder="Mot de passe">
      <button type="submit" class="btn-form">Créer le compte</button>
    </form>
  </div>
  <div class="form-container login-container">
    <form>
      <h1>Se connecter</h1>
      <div class="social-container">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-google-plus-g"></i></a>
        <a href="#"><i class="fab fa-linkedin-in"></i></a>
      </div>
      <span>Je n'ai pas de compte</span>
      <input class="loginEmail" name="email" type="email" placeholder="Email">
      <input class="loginPassword" name="password" type="password" placeholder="Mot de passe">
      <button type="submit" class="btn-form">Se connecter</button>
    </form>
  </div>
  <div class="overlay-container">
    <div class="overlay">
      <div class="overlay-panel overlay-left">
        <h1>Lorem ipsum dolor sit amet consectetur.</h1>
        <p class="pform">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum quibusdam cupiditate exercitationem?</p>
        <button class="btn-form ghost" id="loginUp">Se connecter</button>
      </div>
      <div class="overlay-panel overlay-right">
        <h1>Lorem ipsum dolor sit amet consectetur.</h1>
        <p class="pform">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum quibusdam cupiditate exercitationem?</p>
        <button class="btn-form ghost" id="signUp">Créer un compte</button>
      </div>
    </div>
  </div>
</div>
  `;

export default accueilContent;
