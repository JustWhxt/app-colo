const createDepenses = () => `
<div class="depenses-container">
  <h1>Gestion des Dépenses</h1>
  <form class="depenses-form">

  <input class="nameDepense" type="text" placeholder="Nom de la depense" required></input>

    <input class="risingDepense" type="text" placeholder="Entrez le montant" required/></input>
  
    <select class="userDepense" name="type">
    <option value=""></option>
    <option value="user1">User1</option>
    <option value="user2">User2</option>
    <option value="user2">User3</option>
  </select>

    <select class="categoryDepense" name="type">
      <option value="loyer">Loyer</option>
      <option value="factures">Factures</option>
      <option value="courses">Courses</option>
      <option value="autres">Autres</option>
    </select>

    <button type="submit">Ajouter la dépense</button>
  </form>
</div>
`;

export default createDepenses;
