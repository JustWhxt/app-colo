const createTasks = () => `
  <div class="task-creation-container">
    <h1>Création de Tâches</h1>
    <form class="task-form">
      <input class="titleTask" type="text" placeholder="Titre de la tâche" required></input>
      <textarea class="descriptionTask" placeholder="Description de la tâche" required></textarea>
      <input class="dateTask" type="date" placeholder="Date limite" required />
      <div class="task-form-selects">
        <select required class="priorityTask">
          <option value="">Priorité</option>
          <option value="Haute">Haute</option>
          <option value="Moyenne">Moyenne</option>
          <option value="Basse">Basse</option>
        </select>
        <select required class="categoryTask">
          <option value="">Catégorie</option>
          <option value="Ménagère">Ménagère</option>
          <option value="Dépensière">Dépensière</option>
          <option value="Alimentaire">Alimentaire</option>
        </select>
        <select required class="assignTask">
          <option value="">Assigner à</option>
          <option value="Personne1">Personne 1</option>
          <option value="Personne2">Personne 2</option>
          <option value="Personne3">Personne 3</option>
        </select>
      </div>
      <button type="submit">Créer Tâche</button>
    </form>
  </div>
`;

export default createTasks;
