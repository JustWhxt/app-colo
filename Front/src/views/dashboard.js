import logoTache from '../assets/img/taches.png';
import logoMoney from '../assets/img/money.png';
import logoCalendar from '../assets/img/calendar.png';

const dashboard = () => `
  <div class="dashboard-container">
    <div class="dashboard-tasks-container">
      <div class="container-task">
        <div class="title-container">
          <h1>Vos tâches</h1>
          <img class="logo-tasks" src="${logoTache}" alt="LogoTasks">
        </div>
        <div class="tasks"></div>
      </div>
    </div>
    <div class="dashboard-info-container">
      <div class="dashboard-depenses-container">
        <div class="title-container">
          <h1 class="title-center">Vos dépenses</h1>
          <img class="logo-depenses" src="${logoMoney}" alt="LogoMoney">
        </div>
        <div class="depenses">
        </div>
      </div>
      <div class="dashboard-calendrier-container">
        <div class="title-container">
          <h1 class="title-center">Calendrier Partagé</h1>
          <img class="logo-calendrier" src="${logoCalendar}" alt="LogoCalendar">
        </div>
        <div class="container-calendar">
        <div class="calendar">
          <div class="month">
            <i class="fas fa-angle-left prev"></i>
            <div class="date">
              <h1></h1>
              <p></p>
            </div>
            <i class="fas fa-angle-right next"></i>
          </div>
          <div class="weekdays">
            <div>Sun</div>
            <div>Mon</div>
            <div>Tue</div>
            <div>Wed</div>
            <div>Thu</div>
            <div>Fri</div>
            <div>Sat</div>
          </div>
          <div class="days"></div>
        </div>
      </div>
      </div>
    </div>
  </div>
`;

export default dashboard;
