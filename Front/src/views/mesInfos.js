import getCookie from '../script/getCookie';

const mesInfo = () => {
  const userId = getCookie('userId');
  const userLoggedName = getCookie('userName');

  if (!userId) {
    return 'Veuillez vous connecter pour accéder à vos informations';
  }
  const user = {
    name: userLoggedName,
    email: userLoggedName
  };
  return `
  <div class="container-account">
      <h1>Bienvenue, ${user.name}</h1>
      <h2>Informations personnelles</h2>
      <div class="account-info">
          <div class="info-display">
              <p>Nom : <span id="user-name">${user.name}</span></p>
              <p>Email utilisé : <span id="user-email">${user.email}</span></p>
              <p>Mot de passe utilisé : <span id="user-password">**********</span></p>
          </div>
          <div class="info-modify">
              <form>
                  <label for= "modify-name">Nom :</label>
                  <input type="text" id="modify-name" name="modify-name" placeholder="Name" value="${user.name}">
                  
                  <label for="modify-email">Email :</label>
                  <input type="email" id="modify-email" name="modify-email" placeholder="Email" value="${user.email}">
                  
                  <label for="modify-password">Mot de passe :</label>
                  <input type="password" id="modify-password" placeholder="Password" name="modify-password">
                  
                  <button type="submit" class="btn-modify">Modifier</button>
              </form>
          </div>
      </div>
      <a href="Accueil"><button class="btn-logout">Déconnexion</button></a>
  </div>
  `;
};

export default mesInfo;
