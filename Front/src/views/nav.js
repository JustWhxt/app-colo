import logo from '../assets/img/logo.png';
import getCookie from '../script/getCookie';

const nav = () => {
  const userLoggedIn = getCookie('userId');
  return `<header>
<nav>
    <div class="logo">
        <a href="Accueil"><img src="${logo}" alt="Logo"></a>
    </div>
    <div class="btnhead">
      ${!userLoggedIn ? '<a href="Accueil"><button>ACCUEIL</button></a>' : ''}
      ${userLoggedIn ? '<a href="GestionMenu"><button>GESTION MENU</button></a>' : ''}
      ${userLoggedIn ? '<a href="Dashboard"><button>DASHBOARD</button></a>' : ''}
      <a href="Apropos"><button>A PROPOS</button></a>
      <a href="Informations"><button>MES INFORMATIONS</button></a>
    </div>
</nav>
</header>
`;
};

export default nav;
